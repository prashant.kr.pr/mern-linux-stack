import { UserModel } from '../models/User';

export default (app) => {
    app.get('/v1/users', async (req, res) => {
        const users = await UserModel.find() || [];
        res.send(users);
    });

    app.get('/v1/users/:id', async (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        /**
         * Here we are using try/catch because in case the id is not as per the mongo directives then
         * it will be catched in the Catch statement.
         */
        try {
            const user = await UserModel.findById(req.params.id);
            if (user) {
                res.send(user);
            } else {
                res.send(404).end();
            }
        } catch (error) {
            res.send(404).end();
        }
    });

    /**
     * Here we are handling the post request. "req.body.username" is used to get the post data.
     */
    app.post('/v1/users', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        console.log(req.body);
        const username = req.body.username;
        const email = req.body.email;
        const id = req.body.id;
        const role = req.body.role;

        console.log(`Post Data:- ${id},${username},${email},${role}`);
        res.send(200).end();
    });

    /**
     * PUT is used to update the data.
     */
    app.put('/v1/users/:id', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        const id = req.body.id;
        const username = req.body.username;
        const email = req.body.email;
        const role = req.body.role;
        console.log(`PUT Data:- ${id},${username},${email},${role}`);
        res.send(200).end();
    });

    app.delete('/v1/users/:id', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        console.log(`Delete Request:- ${req.body.id}`);
        res.send(200).end();
    })
}
import { ProductModel } from "../models/Product";

export default (app) => {
    app.get('/v1/products', async (req, res) => {
        const { categories } = req.query;
        /**
         * "split" will break the comma separated string to Array.
         */
        const categoryList = categories ? categories.split(',') : [];
        const products = await ProductModel.find(
            categoryList.length > 0 ?
                { categories: { $in: categoryList } } :
                undefined
        ) || [];
        // const products = await ProductModel.find() || [];
        res.send(products);
    });

    app.get('/v1/products/:id', async (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        try {
            const product = await ProductModel.findById(req.params.id);
            if (product) {
                res.send(product);
            } else {
                res.send(404).end();
            }
        } catch (error) {
            res.send(404).end();
        }
    });
    /**
     * This is for creating new entry for products using ProductModel. The data of product is comming from 
     * post request.
     */
    app.post('/v1/products', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        const product = ProductModel.create(req.body);
        if (product) {
            res.send(200).end();
        } else {
            res.send(500).end();
        }
    });

    app.put('/v1/products/:id', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        ProductModel.findByIdAndUpdate(
            req.params.id,
            req.body,
            (err) => {
                if (err) {
                    res.send(500).end();
                } else {
                    res.send(200).end();
                }
            }
        )
    });

    app.delete('/v1/products/:id', (req, res) => {
        if (!req.isAdmin) {
            return res.status(403).end();
        }
        ProductModel.findByIdAndDelete(
            req.params.id,
            (err) => {
                if (err) {
                    res.send(500).end();
                } else {
                    res.send(200).end();
                }
            }
        )
    });
}
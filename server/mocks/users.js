import UserModal from '../models/User';  

const user1 = new UserModal({
    id: '11',
    username:'Prashant',
    email:'abc@gmail.com',
    role:'admin'
});

const user2 = new UserModal({
    id: '22',
    username:'secondName',
    email:'xyz@gmail.com',
    role:'customer'
});

const users = [user1.getData(), user2.getData()];

export default users;
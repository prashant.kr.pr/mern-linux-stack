import mongoose from 'mongoose';
var Schema = mongoose.Schema;

// export default class Product {
//     /**
//      * 
//      * @param {String} id
//      * @param {String} name
//      * @param {Number} price
//      * @param {Array.<String>} images 
//      */
//     constructor({ id, name, price, images }) {
//         this._id = id;
//         this._name = name;
//         this._price = price;
//         this._images = images;
//     }

//     /**
//      * @returns {String}
//      */
//     getId = () => this._id;

//     /**
//      * @returns {String}
//      */
//     getName = () => this._name;

//     /**
//      * @returns {String}
//      */
//     getPrice = () => this._price;

//     /**
//      * @returns {Array<String>}
//      */
//     getImages = () => this._images;

//     /**
//      * @returns {{id:string, name:string, price:number, images:Array<String>}}
//      */
//     getData = () => ({
//         id: this._id,
//         name: this._name,
//         price: this._price,
//         images: this._images
//     })
// }

export const ProductSchema = new Schema({
    name: String,
    price: Number,
    images: [String],
    categories: [String]
});

export const ProductModel = mongoose.model('Product', ProductSchema);
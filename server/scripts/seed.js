const { MongoClient, ObjectID } = require('mongodb');
import { users, products } from './data';
const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const UserSchema = new Schema({
    email:String,
    username:String,
    role:String
});

const UserModel = mongoose.model('User', UserSchema);

var users = [
    new UserModel({
        username: 'firstusername',
        email: 'firstemail@g.com',
        role: 'customer'
    })
];

MongoClient.connect('mongodb://localhost:27017/mern',{ useNewUrlParser: true }, (err, client) => {
    if (err) {
        return console.log('Unable to connect.');
    }
    console.log('Connection Successfullxc');
    const db = client.db('mern');

    var done = 0;
    for (var i = 0; i < users.length; i++) {
        console.log(users[i]);

        db.collection('User').insert({
            username: users[i]['username'],
            email: users[i]['email'],
            role: users[i]['role']
        },
            (err, result) => {
                return console.log('Unable to insert into User table.', err);
            });
        done++;
        if (done === users.length) {
            client.close();
        }
    }
});
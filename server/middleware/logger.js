/**
 * As it is single responsibility function so it is exported as Anonymus function.
 */
export default (req, res, next) => {
    console.log(req.user);
    console.log(
      '=> ',
      req.method,
      req.originalUrl,
      ' || ',
      'isAuthenticated: ',
      typeof req.user === 'object',
      'is Admin: ',
      req.user && req.user.data && req.user.data.role === 'admin'
    );
    next();
  };
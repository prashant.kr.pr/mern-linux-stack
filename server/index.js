import express from 'express';
import bodyParser from 'body-parser';
const app = express();

import cors from 'cors';
import users from '../server/mocks/users';
import logger from '../server/middleware/logger';
import withAuthenticated from '../server/middleware/withAuthentication';
import db from '../server/db/index';
import getUserRoutes  from "../server/routes/users";
import getProductRoutes from "../server/routes/products";
// import { UserModel } from '../server/models/User';
// import Product, { ProductModel } from '../server/models/Product';
import getAuthRoutes from '../server/routes/auth';

/**
 * Middleware are always called in chain and in the order they are defined.
 */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(withAuthenticated);
app.use(logger);
app.use(cors());
getUserRoutes(app);
getProductRoutes(app);
getAuthRoutes(app);

// const port = 8585;

/**
 * Now the port number will be read from the environment. In this case we can either add the 
 * port number in the start script or can provide the port number from the browser or cli. This makes the code more dynamic.
 */
const port = process.env.PORT;
app.get('/', (req, res) => res.send({
    datetime: new Date().toJSON()
}));

app.get('/ping', (req, res) => res.send('pong'));

app.listen(port, () => {
    console.log(`Conection established on port:- ${port}`);
});
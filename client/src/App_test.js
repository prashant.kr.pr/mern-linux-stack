// This is just a testing APP page
import React, { Component } from 'react';
import './App.css';
import DynamicCounter from './components/DynamicCounter';

class App extends Component {
  state = {
    Apples: 0,
    Orange: 0,
    Lemons: 0
  };

  increment = (key) => () => {
    this.setState({
      [key]: this.state[key] + 1,
    });
  }

  decrement = (key) => () => {
    this.setState({
      [key]: this.state[key] - 1,
    });
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <DynamicCounter
            label="Apples"
            max={5}
            min={1}
            increment={this.increment}
            decrement={this.decrement}
            value={this.state.Apples}
          />
          <DynamicCounter
            label="Orange"
            max={3}
            min={-1}
            increment={this.increment}
            decrement={this.decrement}
            value={this.state.Orange}
          />
          <DynamicCounter
            label="Lemons"
            max={6}
            min={-2}
            increment={this.increment}
            decrement={this.decrement}
            value={this.state.Lemons}
          />
          
          {/* Here we are showing the err msg. */}
          {
            (
              this.state.Apples < 0 &&
              this.state.Orange < 0 &&
              this.state.Lemons < 0
            ) ?
              <p>All values are less than Zero</p> :
              null
          }
        </div>
      </div>
    );
  }
}

export default App;

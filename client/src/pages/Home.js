import React, { Component } from 'react';
// import products from '../data/Products';
import ProductList from '../components/product/ProductList'
import { getProducts } from '../api/Products';
import LoadingIndicator from '../components/LoadingIndicator';

class Home extends Component {
    state = { products: [], loading:true };

    componentDidMount = async () => {
        console.log('Component mounted.');
        const products = await getProducts() || [];
        this.setState({ products, loading:false });
    }
    // componentWillUnmount = () => {
    //     console.log('Component Unmounted.');
    // }

    // componentDidUpdate = () => {
    //     console.log('This is component did update method.');
    // }

    // componentWillUpdate = () => {
    //     console.log('This is component will update method.');
    // }

    render() {
        return (
            this.state.loading ? 
            <LoadingIndicator /> :
            <ProductList products={this.state.products} />
        );
    }
}

export default Home;
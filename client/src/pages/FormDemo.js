import React, { Component } from 'react';
import Form from './../components/inputs/Form';
import TextInput from './../components/inputs/TextInput';
import PasswordInput from './../components/inputs/PasswordInput';
import CheckboxInput from './../components/inputs/CheckboxInput';
import { PrimaryButton } from '../components/Button';

class FormDemo extends Component {
    state = {
        firstname: "",
        lastname: "",
        password: "",
        sendemail: true
    }
    handleTextChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleBooleanChange = e => {
        this.setState({
            [e.target.name]: e.target.checked
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        console.log('hello');
    }
    render() {
        return (
            <div className="App">
                {console.log('hereerer')}
                <Form onSubmit={this.handleSubmit}>
                    <TextInput
                        label="First Name"
                        value={this.state.firstname}
                        name="firstname"
                        onChange={this.handleTextChange}
                    />
                    <TextInput
                        label="Last Name"
                        value={this.state.lastname}
                        name="lastname"
                        onChange={this.handleTextChange}
                    />
                    <PasswordInput
                        label="Password"
                        value={this.state.password}
                        name="password"
                        onChange={this.handleTextChange}
                    />
                    <CheckboxInput
                        label="Can we send you promo email?"
                        checked={this.state.sendemail}
                        name="sendemail"
                        onChange={this.handleBooleanChange}
                    />
                    <PrimaryButton>
                        Submit
                    </PrimaryButton>
                </Form>
            </div>
        );
    }
}

export default FormDemo;
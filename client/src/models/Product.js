export default class Product {
    /**
     * 
     * @param {String} _id
     * @param {String} name
     * @param {Number} price
     * @param {Array.<String>} images 
     */
    constructor({ _id, name, price, images }) {
        this._id = _id;
        this._name = name;
        this._price = price;
        this._images = images;
    }

    /**
     * @returns {String}
     */
    getId = () => this._id;

    /**
     * @returns {String}
     */
    getName = () => this._name;

    /**
     * @returns {String}
     */
    getPrice = () => this._price;

    /**
     * @returns {String}
     */
    getFormattedPrice = () => 
        `$${String(this._price / 100)}`;
    

    /**
     * @returns {Array<String>}
     */
    getImages = () => this._images;

    /**
     * @returns {{_id:string, name:string, price:number, formattedPrice:String, images:Array<String>}}
     */
    getData = () => ({
        _id: this._id,
        name: this._name,
        price: this._price,
        formattedPrice: this.getFormattedPrice(),
        images: this._images
    })
}
import React, { Component } from 'react';
import SpicyDataTable from 'spicy-datatable'

class DataTable extends Component {
    render() {
        return (
            <div>
                <SpicyDataTable
                    tableKey={this.props.tableKey}
                    columns={this.props.columns}
                    rows={this.props.data}
                />
            </div>
        );
    }
}

export default DataTable;
import React, { Component } from 'react';
import BaseInputs from './BaseInputs';

class PasswordInput extends Component {
    render() {
        return (
            <BaseInputs
                {...this.props}
                type="password" 
            />
        );
    }
}

export default PasswordInput;
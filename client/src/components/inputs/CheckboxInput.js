import React, { Component } from 'react';
import BaseInputs from './BaseInputs';

class CheckboxInput extends Component {
    render() {
        return (
            <BaseInputs
                {...this.props}
                type="checkbox" 
            />
        );
    }
}

export default CheckboxInput;
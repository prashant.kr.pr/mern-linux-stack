import React, { Component } from 'react';
import './ProductCard.css';
import TopProductLabel from '../TopProductLabel';
import { SecondaryButton } from '../Button';

class ProductCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: this.props.images[0]
        };
    }

    handleMouseOver = () => {
        if (this.props.images.length > 1) {
            this.setState({
                image: this.props.images[1]
            })
        }
    }

    handleMouseLeave = () => {
        this.setState({
            image: this.props.images[0]
        })
    }

    render() {
        return (
            <div
                className="ProductCard"
                style={
                    this.props.pull ?
                        { alignSelf: 'flex-end' } :
                        null
                }
            >
                <img src={this.state.image}
                    onMouseOver={this.handleMouseOver}
                    onMouseLeave={this.handleMouseLeave}
                    alt="Hello"
                />
                <h3>{this.props.name}</h3>
                <p>
                    {this.props.price}
                    {this.props.isFeatured && <TopProductLabel />}
                </p>
                {this.props.withRemoveButton &&
                    <SecondaryButton onClick={this.props.onRemove}>
                        Remove
                    </SecondaryButton>
                }
            </div>
        );
    }
}

export default ProductCard;